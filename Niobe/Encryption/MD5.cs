﻿using System.Text;

namespace Niobe.Encryption {
    public class MD5 {
        /* Static methods */

        public static string Encrypt(string source) {
            string hash;

            using(System.Security.Cryptography.MD5 md5_hash = System.Security.Cryptography.MD5.Create()) {
                byte[] data = md5_hash.ComputeHash(Encoding.UTF8.GetBytes(source));

                StringBuilder string_builder = new StringBuilder();

                for(int i = 0; i < data.Length; i++) {
                    string_builder.Append(data[i].ToString("x2"));
                }

                hash = string_builder.ToString();
            }

            return hash;
        }
    }
}
