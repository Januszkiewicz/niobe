﻿using System.IO;

using System.Net;

using System.Text;

namespace Niobe {
    public class WebHandler {
        /* Instance variables */

        public string url = null;

        public CookieContainer cookies = new CookieContainer();

        /* Instance static methods */

        public static string Request(string url, CookieContainer cookies, string query_string = null, string[] data = null, string method = "POST") {
            string response = null;

            if(method == "GET" && query_string != null) {
                url += query_string;
            }

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            request.Method = method;

            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "sv-SE;q=0.6,sv;q=0.4");

            if(method == "POST" && data != null) {
                byte[] bytes = Encoding.ASCII.GetBytes(string.Join("&", data));

                request.ContentLength = bytes.Length;

                using(Stream data_stream = request.GetRequestStream()) {
                    data_stream.Write(bytes, 0, bytes.Length);
                }
            }

            HttpWebResponse web_response = request.GetResponse() as HttpWebResponse;

            if(web_response.StatusCode == HttpStatusCode.OK) {
                Stream stream = web_response.GetResponseStream();

                using(StreamReader reader = new StreamReader(stream)) {
                    response = reader.ReadToEnd();
                }
            }

            return response;
        }

        /* Instance methods */

        public CookieContainer GetCookies(string url) {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            request.Method = "GET";

            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "sv-SE;q=0.6,sv;q=0.4");

            request.CookieContainer = this.cookies;

            HttpWebResponse http_response = request.GetResponse() as HttpWebResponse;

            Stream stream = http_response.GetResponseStream();

            StreamReader reader = new StreamReader(stream);

            reader.ReadToEnd();

            reader.Close();

            http_response.Close();

#pragma warning disable CS1717
            this.cookies = this.cookies;
#pragma warning restore CS1717

            return this.cookies;
        }
    }
}
