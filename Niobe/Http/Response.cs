﻿namespace Niobe.Http {
    public class Response {
        /* Instance variables */

        public System.Net.HttpStatusCode http_code = 0;

        public string data = null;

        public System.IO.Stream stream = null;
    }
}
