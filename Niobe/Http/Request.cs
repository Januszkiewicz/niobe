﻿#define DEBUG

using System;

using System.IO;

using System.IO.Compression;

using System.Net;

using System.Net.Http;

using System.Text;

using System.Web;

namespace Niobe.Http {
    public class Request {
        /* Instance variables */

        public string Url = null;

        public CookieContainer Cookies = new CookieContainer();

        /* Static methods */

        public static Response GetResponseStream(string url, string query_string = null) {
            Response response = new Response();

            HttpClientHandler handler = new HttpClientHandler();

            Uri uri = new Uri(url + "?" + query_string);

            using(HttpClient client = new HttpClient(handler)) {
                HttpRequestMessage request = new HttpRequestMessage() {
                    Method = new HttpMethod("GET"),
                    RequestUri = uri
                };

                System.Diagnostics.Debug.WriteLine("GET " + url + "?" + query_string);

                using(HttpResponseMessage http_response = client.SendAsync(request).Result) {
                    response.http_code = http_response.StatusCode;

                    if(http_response.IsSuccessStatusCode) {
                        response.stream = http_response.Content.ReadAsStreamAsync().Result;
                    }
                }
            }

            return response;
        }

        /* Compression static methods [todo: move to separate Compression.cs class] */

        public static void CopyTo(Stream source, Stream destination) {
            byte[] bytes = new byte[4096];

            int count;

            while((count = source.Read(bytes, 0, bytes.Length)) != 0) {
                destination.Write(bytes, 0, count);
            }
        }

        public static string Unzip(byte[] bytes) {
            using(MemoryStream memory_stream = new MemoryStream(bytes)) {
                using(MemoryStream memory_stream_output = new MemoryStream()) {
                    using(GZipStream gzip_stream = new GZipStream(memory_stream, CompressionMode.Decompress)) {
                        CopyTo(gzip_stream, memory_stream_output);
                    }

                    return Encoding.UTF8.GetString(memory_stream_output.ToArray());
                }
            }
        }

        /* Instance methods */

        public Response Get(string url, string query_string = null, bool minimize = false) {
            Response response = new Response();

            HttpClientHandler handler = new HttpClientHandler {
                CookieContainer = this.Cookies
            };

            Uri uri = new Uri(this.Url + url + "?" + query_string);

#if DEBUG
            Console.WriteLine("[DEBUG] Request url: {0}", this.Url + url + "?" + query_string);
#endif

            using(HttpClient client = new HttpClient(handler)) {
                HttpRequestMessage request = new HttpRequestMessage() {
                    Method = new HttpMethod("GET"),
                    RequestUri = uri
                };

                request.Headers.Add("Accept", "text/html, application/xhtml+xml, application/xml;q=0.9, image/webp, */*;q=0.8");

                using(HttpResponseMessage http_response = client.SendAsync(request).Result) {
                    response.http_code = http_response.StatusCode;

#if DEBUG
                    Console.WriteLine("[DEBUG] Request http response: {0}", http_response.StatusCode);

                    Console.WriteLine("[DEBUG] Shall continue: {0}", http_response.IsSuccessStatusCode ? "Yes" : "no");
#endif

                    if(http_response.IsSuccessStatusCode) {
                        response.data = http_response.Content.ReadAsStringAsync().Result;

                        response.data = HttpUtility.HtmlDecode(response.data);

                        if(minimize) {
                            response.data = response.data.Replace("\t", " ");

                            response.data = response.data.Replace(Environment.NewLine, " ");

                            while(response.data.IndexOf("  ") != -1) {
                                response.data = response.data.Replace("  ", " ");
                            }
                        }
                    }
                }
            }

            return response;
        }
    }
}
