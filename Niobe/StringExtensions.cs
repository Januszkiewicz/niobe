﻿using System;

namespace Niobe {
    public static class StringExtensions {
        public static string PadBoth(this string instance, int length, char character = '-') {
            int paddingLeft = length + instance.Length;

            return instance.PadLeft(paddingLeft, character).PadRight(length + paddingLeft, character);
        }
    }
}