﻿using System;

namespace Niobe {
    public static class DateTimeExtensions {
        /* Static methods */

        public static double UnixTimestamp(this DateTime date_time) {
            return (TimeZoneInfo.ConvertTimeToUtc(date_time) - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}
