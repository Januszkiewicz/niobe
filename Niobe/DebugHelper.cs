﻿using System;

namespace Niobe {
    public static class DebugHelper {
        /* Static methods */

        public static void OnException(object sender, UnhandledExceptionEventArgs arguments) {
            Exception exception = (Exception)arguments.ExceptionObject;

            while(true) {
                if(exception != null) {
                    Console.WriteLine("EXCEPTION".PadBoth(40, '-'));

                    Console.WriteLine("\nSource:\n{0}", exception.Source);

                    Console.WriteLine("\nTarget site:\n{0}", exception.TargetSite);

                    Console.WriteLine("\nMessage:\n{0}", exception.Message);

                    Console.WriteLine("\nData:\n{0}", Niobe.Json.Encode(exception.Data));

                    Console.WriteLine("\nHelp link:\n{0}", exception.HelpLink);

                    Console.WriteLine("\nTrace:\n{0}", exception.StackTrace);

                    Console.WriteLine('\n' + "---------".PadBoth(40, '-') + '\n');
                }

                exception = exception.InnerException;

                if(exception == null) {
                    break;
                }
            }
        }
    }
}
