﻿using Newtonsoft.Json;

namespace Niobe {
    public class Json {
        /* Static methods */

        public static string Encode(object obj) {
            return JsonConvert.SerializeObject(obj);
        }

        public static T Decode<T>(string value) {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}