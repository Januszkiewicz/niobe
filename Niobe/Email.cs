﻿using System.Net;

using System.Net.Mail;

namespace Niobe {
    public class Email {
        /* Static methods */

        public static void Send(string host, string username, string password, string to, string subject, string body) {
            MailMessage mail = new MailMessage(username, to);

            SmtpClient client = new SmtpClient();

            NetworkCredential basicCredential = new NetworkCredential(username, password);

            client.Port = 25;

            client.DeliveryMethod = SmtpDeliveryMethod.Network;

            client.UseDefaultCredentials = false;

            client.Credentials = basicCredential;

            client.Host = host;

            mail.Subject = subject;

            mail.Body = body;

            client.Send(mail);
        }
    }
}
