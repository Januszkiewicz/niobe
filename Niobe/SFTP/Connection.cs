﻿using System;

using System.Xml;

using WinSCP;

namespace Niobe.SFTP {
    public class Connection {
        /* Instance variables */

        private readonly Protocol protocol = Protocol.Sftp;

        private readonly WinSCP.SessionOptions session_options;

        private readonly bool log_active = false;

        /* Constructor method */

        public Connection(XmlNode node) {
            if(node == null) {
                return;
            }

            this.log_active = Convert.ToBoolean(node.Attributes["enable_log"]?.Value);

            this.session_options = new SessionOptions {
                Protocol = this.protocol,
                HostName = node.Attributes["hostname"]?.Value,
                UserName = node.Attributes["username"]?.Value,
                Password = node.Attributes["password"]?.Value,
            };

            if(node.Attributes["tunnel"]?.Value != null) {
                this.session_options.AddRawSettings("Tunnel", node.Attributes["tunnel"].Value);
            }

            if(node.Attributes["tunnel_hostname"]?.Value != null) {
                this.session_options.AddRawSettings("TunnelHostName", node.Attributes["tunnel_hostname"].Value);
            }

            if(node.Attributes["tunnel_username"]?.Value != null) {
                this.session_options.AddRawSettings("TunnelUserName", node.Attributes["tunnel_username"].Value);
            }

            if(node.Attributes["tunnel_password_plain"]?.Value != null) {
                this.session_options.AddRawSettings("TunnelPasswordPlain", node.Attributes["tunnel_password_plain"].Value);
            }

            if(node.Attributes["tunnel_password_plain"]?.Value != null) {
                this.session_options.AddRawSettings("TunnelHostKey", node.Attributes["tunnel_host_key"].Value);
            }
        }

        /* Instance methods */

        public bool UploadFiles(string source, string destination) {
            using(Session session = new Session()) {
                if(this.log_active) {
                    session.SessionLogPath = @"C:\Users\agaci\Desktop\text.txt";
                }

                this.session_options.SshHostKeyFingerprint = session.ScanFingerprint(this.session_options, "SHA-256");

                session.Open(this.session_options);

                session.PutFiles(source, destination, false, new TransferOptions { OverwriteMode = OverwriteMode.Overwrite, FilePermissions = new FilePermissions() { Octal = "0777" } }).Check();

                session.Close();
            }

            return true;
        }
    }
}
