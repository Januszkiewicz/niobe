﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Niobe.MySql.Compiler {
    public static class Helper {
        public static bool IsArray(object value) {
            if(value is string) {
                return false;
            }

            if(value is byte[]) {
                return false;
            }

            return value is IEnumerable;
        }

        public static IEnumerable<object> Flatten(IEnumerable<object> array) {
            foreach(object item in array) {
                if(IsArray(item)) {
                    foreach(object sub in item as IEnumerable) {
                        yield return sub;
                    }
                }
                else {
                    yield return item;
                }

            }
        }

        public static string ReplaceAll(string subject, string match, Func<int, string> callback) {
            if(string.IsNullOrWhiteSpace(subject) || !subject.Contains(match)) {
                return subject;
            }

            string[] splitted = subject.Split(new[] { match }, StringSplitOptions.None);

            return splitted.Skip(1).Select((item, index) => callback(index) + item).Aggregate(splitted.First(), (left, right) => left + right);
        }
    }
}
