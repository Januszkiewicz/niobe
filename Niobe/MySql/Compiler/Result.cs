﻿using System;

using System.Collections.Generic;

using System.Globalization;

using System.Linq;

namespace Niobe.MySql.Compiler {
    public class Result {
        public Query Query;

        public string Sql = "";

        public List<object> Bindings = new List<object>();

        public Dictionary<string, object> NamedBindings = new Dictionary<string, object>();

        private static readonly Type[] NumericTypes = {
            typeof(int),
            typeof(long),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(short),
            typeof(ushort),
            typeof(ulong),
        };

        public override string ToString() {
            List<object> deepParameters = Helper.Flatten(this.Bindings).ToList();

            return Helper.ReplaceAll(this.Sql, "?", i => {
                if(i >= deepParameters.Count) {
                    throw new Exception($"Failed to retrieve a binding at index {i}, the total bindings count is {this.Bindings.Count}");
                }

                object value = deepParameters[i];

                return this.ChangeToSqlValue(value);
            });
        }

        private string ChangeToSqlValue(object value) {
            if(value == null) {
                return "NULL";
            }

            if(NumericTypes.Contains(value.GetType())) {
                return Convert.ToString(value, CultureInfo.InvariantCulture);
            }

            if(value is DateTime date) {
                string format = date.Date == date ? "yyyy-MM-dd" : "yyyy-MM-dd HH:mm:ss";

                return "\"" + date.ToString(format) + "\"";
            }

            if(value is bool boolean) {
                return boolean ? "TRUE" : "FALSE";
            }

            return "\"" + value.ToString() + "\"";
        }
    }
}
