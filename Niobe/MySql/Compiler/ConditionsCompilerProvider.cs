﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Niobe.MySql.Compiler {
    internal class ConditionsCompilerProvider {
        private readonly Type compilerType;

        private readonly Dictionary<string, MethodInfo> methodsCache = new Dictionary<string, MethodInfo>();

        private readonly object methodLock = new object();

        public ConditionsCompilerProvider(QueryCompiler compiler) {
            this.compilerType = compiler.GetType();
        }

        public MethodInfo GetMethodInfo(Type clause_type, string method_name) {
            string cacheKey = method_name + "::" + clause_type.FullName;

            lock(this.methodLock) {
                if(this.methodsCache.ContainsKey(cacheKey)) {
                    return this.methodsCache[cacheKey];
                }

                return this.methodsCache[cacheKey] = this.FindMethodInfo(clause_type, method_name);
            }
        }

        private MethodInfo FindMethodInfo(Type clause_type, string method_name) {
            MethodInfo methodInfo = this.compilerType.GetRuntimeMethods().FirstOrDefault(x => x.Name == method_name);

            if(methodInfo == null) {
                throw new Exception($"Failed to locate a compiler for '{method_name}'.");
            }

            if(clause_type.IsConstructedGenericType && methodInfo.GetGenericArguments().Any()) {
                methodInfo = methodInfo.MakeGenericMethod(clause_type.GenericTypeArguments);
            }

            return methodInfo;
        }
    }
}
