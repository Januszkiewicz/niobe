﻿using Niobe.MySql.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Niobe.MySql {
    public class QueryCompiler {
        /* Instance Variables */

        private readonly ConditionsCompilerProvider _compileConditionMethodsProvider;

        protected readonly HashSet<string> Operators = new HashSet<string>
        {
            "=", "<", ">", "<=", ">=", "<>", "!=", "<=>",
            "BETWEEN", "NOT BETWEEN",
            "LIKE", "NOT LIKE",
            "ILIKE", "NOT ILIKE",
            "LIKE BINARY", "NOT LIKE BINARY",
            "RLIKE", "NOT RLIKE",
            "REGEXP", "NOT REGEXP",
            "SIMILAR TO", "NOT SIMILAR TO"
        };

        protected string ParameterPrefix = "@p";

        protected string ParameterPlaceholder = "?";

        /* Constructor */

        public QueryCompiler() {
            this._compileConditionMethodsProvider = new ConditionsCompilerProvider(this);
        }

        /* Instance Compile conditions provider method */

        protected virtual MethodInfo FindCompilerMethodInfo(Type clause_type, string method_name) {
            return this._compileConditionMethodsProvider.GetMethodInfo(clause_type, method_name);
        }

        /* Instance Compile methods */

        public Compiler.Result Compile(Query query) {
            Compiler.Result result = new Compiler.Result {
                Query = query
            };

            if(query.statement == "INSERT") {
                this.Insert(result);
            }
            else if(query.statement == "SELECT") {
                this.Select(result);
            }

            this.PrepareResult(result);

            return result;
        }

        private void Insert(Compiler.Result result) {
            Clauses.From clause = result.Query.Clauses.Where((item) => item.component == "from").Cast<Clauses.From>().First();


        }

        private void Select(Compiler.Result result) {
            List<string> parts = new[] {
                this.CompileColumns(result),
                this.CompileFrom(result),
                this.CompileWhere(result),
                this.CompileLimit(result)
            }.Where(part => part != null).ToList();

            result.Sql = string.Join(" ", parts);
        }

        protected Dictionary<string, object> generateNamedBindings(object[] bindings) {
            return Helper.Flatten(bindings).Select((v, i) => new { i, v }).ToDictionary(x => this.ParameterPrefix + x.i, x => x.v);
        }

        protected Result PrepareResult(Result result) {
            result.NamedBindings = this.generateNamedBindings(result.Bindings.ToArray());

            result.Sql = Helper.ReplaceAll(result.Sql, this.ParameterPlaceholder, i => this.ParameterPrefix + i);

            return result;
        }

        /* Instance compile component */

        private string CompileColumns(Compiler.Result result) {
            List<Clauses.Column> columns = result.Query.Clauses.Where((item) => item.component == "column").Cast<Clauses.Column>().ToList();

            return "SELECT " + (columns.Any() ? string.Join(",", columns) : "*");
        }

        private string CompileFrom(Compiler.Result result) {
            Clauses.From clause = result.Query.Clauses.Where((item) => item.component == "from").Cast<Clauses.From>().First();

            if(clause == null) {
                throw new System.Exception("Invalid query. Table must be specified.");
            }

            return clause.ToString();
        }

        private string CompileLimit(Compiler.Result result) {
            Clauses.Limit clause = result.Query.Clauses.Where((item) => item.component == "limit").Cast<Clauses.Limit>().FirstOrDefault();

            if(clause == null) {
                return null;
            }

            return clause.ToString();
        }

        private string CompileWhere(Result result) {
            List<Clauses.AbstractCondition> conditions = result.Query.Clauses.Where((item) => item.component == "where").Cast<Clauses.AbstractCondition>().ToList();

            if(!conditions.Any()) {
                return null;
            }

            string compiled = this.CompileConditions(result, conditions);

            return "WHERE " + compiled;
        }

        private string CompileConditions(Result result, List<Clauses.AbstractCondition> conditions) {
            List<string> items = new List<string>();

            for(int i = 0; i < conditions.Count; i++) {
                string compiled = this.CompileCondition(result, conditions[i]);

                if(compiled == null) {
                    continue;
                }

                string boolean_operator = i == 0 ? "" : (conditions[i].is_and ? "AND " : "OR ");

                items.Add(boolean_operator + compiled);
            }

            return string.Join(" ", items);
        }

        /* Instance compile condition component */

        protected virtual string CompileCondition(Result result, Clauses.AbstractCondition condition) {
            System.Type conditionType = condition.GetType();

            string name = conditionType.Name.Substring(0, conditionType.Name.IndexOf("Condition"));

            string method = "Compile" + name + "Condition";

            MethodInfo methodInfo = this.FindCompilerMethodInfo(conditionType, method);

            try {
                return (string)methodInfo.Invoke(this, new object[] { result, condition });
            }
            catch(Exception exception) {
                throw new Exception($"Failed to invoke '{method}'", exception);
            }
        }

        private string CompileNestedCondition(Result result, Clauses.Conditions.NestedCondition condition) {
            List<Clauses.AbstractCondition> conditions = condition.Query.Clauses.Where((item) => item.component == "where").Cast<Clauses.AbstractCondition>().ToList();

            return 0 < conditions.Count ? "(" + this.CompileConditions(result, conditions) + ")" : "";
        }

        private string CompileBasicCondition(Result result, Clauses.Conditions.BasicCondition condition) {
            return this.Escape(condition.Column) + " " + this.CheckOperator(condition.Operator) + " " + this.Parameter(result, condition.Value);
        }

        /* Instance Helper component */

        protected string CheckOperator(string op) {
            op = op.ToUpperInvariant();

            if(!this.Operators.Contains(op)) {
                throw new InvalidOperationException($"The operator '{op}' is not valid.");
            }

            return op;
        }

        public virtual string Parameter(Result ctx, object parameter) {
            ctx.Bindings.Add(parameter);

            return "?";
        }
    }
}
