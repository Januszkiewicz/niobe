﻿namespace Niobe.MySql {
    public static class QueryCompilerExtensions {
        /* Static methods */

        public static string Escape(this QueryCompiler query_compiler, string value) {
            return '`' + value + '`';
        }
    }
}
