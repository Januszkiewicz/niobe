﻿using Renci.SshNet;

using System;

using System.Xml;

namespace Niobe.MySql {
    public class SshConnection {
        /* Instance variables */

        private readonly PasswordConnectionInfo connection;

        /* Constructor methods */

        public SshConnection(XmlNode node) {
            if(node.Attributes?["host"] != null && node.Attributes?["username"] != null && node.Attributes?["password"] != null) {
                this.connection = new PasswordConnectionInfo(node.Attributes["host"].Value, node.Attributes["username"].Value, node.Attributes["password"].Value);
            }

            if(node.Attributes["timeout"] != null) {
                this.connection.Timeout = TimeSpan.FromSeconds(Convert.ToDouble(node.Attributes["timeout"].Value));
            }
        }
    }
}
