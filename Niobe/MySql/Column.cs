﻿namespace Niobe.MySql {
    public class Column {
        public readonly string Name;

        public readonly string Alias = null;

        public override string ToString() {
            return this.Name + (this.Alias != null ? " AS " + this.Alias : "");
        }
    }
}
