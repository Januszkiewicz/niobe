﻿namespace Niobe.MySql {
    public static class QueryExtensions {
        /* Static methods */

        public static string Get(this Query query) {
            return query.statement;
        }
    }
}
