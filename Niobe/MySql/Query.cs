﻿using System.Collections.Generic;

using System.Linq;

namespace Niobe.MySql {
    public class Query {
        /* Private instance variables */

        public string statement { get; private set; } = "SELECT";

        public string Alias;

        public List<AbstractClause> Clauses = new List<AbstractClause>();

        public List<Clauses.AbstractCondition> Conditions = new List<Clauses.AbstractCondition>();

        /* Constructor methods */

        public Query() { }

        public Query(string table) {
            this.Clauses.Add(new Clauses.From { Table = table });
        }

        /* Clause methods */

        public Query Insert(IEnumerable<KeyValuePair<string, object>> pairs) {
            this.statement = "INSERT";

            this.Clauses.Add(new Clauses.Insert { 
                Columns = pairs.Select(item => item.Key).ToList(), 
                Values = pairs.Select(item => item.Value).ToList()
            });

            return this;
        }

        public Query Select(params string[] columns) {
            foreach(string column in columns) {
                this.Clauses.Add(new Clauses.Column { name = column });
            }

            return this;
        }

        public Query Limit(int limit) {
            this.Clauses.Add(new Clauses.Limit { amount = 1 });

            return this;
        }

        /* Condition clause methods */

        public Query Where(string column, string op, object value) {
            this.Clauses.Add(new Clauses.Conditions.BasicCondition { Column = column, Operator = op, Value = value });

            return this;
        }

        public Query Where(string column, object value) {
            this.Clauses.Add(new Clauses.Conditions.BasicCondition { Column = column, Operator = "=", Value = value });

            return this;
        }

        public Query Where(System.Func<Query, Query> callback) {
            this.Clauses.Add(new Clauses.Conditions.NestedCondition { Query = callback.Invoke(new Query()) });

            return this;
        }

        public Query OrWhere(string column, string op, object value) {
            this.Clauses.Add(new Clauses.Conditions.BasicCondition { Column = column, Operator = "=", Value = value, isAnd = false });

            return this;
        }

        public Query OrWhere(string column, object value) {
            this.Clauses.Add(new Clauses.Conditions.BasicCondition { Column = column, Operator = "=", Value = value, isAnd = false });

            return this;
        }

        public Query OrWhere(System.Func<Query, Query> callback) {
            this.Clauses.Add(new Clauses.Conditions.NestedCondition { Query = callback.Invoke(new Query()), isAnd = false });

            return this;
        }
    }
}