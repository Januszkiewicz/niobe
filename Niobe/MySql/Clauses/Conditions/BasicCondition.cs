﻿namespace Niobe.MySql.Clauses.Conditions {
    public class BasicCondition : AbstractCondition {
        public string Column;

        public string Operator;

        public object Value;

        public bool isAnd = true;
    }
}
