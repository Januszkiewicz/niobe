﻿namespace Niobe.MySql.Clauses {
    internal class From : AbstractClause {
        public override string component { get; set; } = "from";

        public string Alias;

        public string Table;

        public override string ToString() {
            return "FROM " + (this.Alias != null ? this.Table + " AS " + this.Alias : this.Table);
        }
    }
}