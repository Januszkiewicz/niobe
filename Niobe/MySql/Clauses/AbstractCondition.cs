﻿namespace Niobe.MySql.Clauses {
    public abstract class AbstractCondition : AbstractClause {
        public override string component { get; set; } = "where";

        public bool is_and { get; set; } = true;

        public bool is_not { get; set; } = false;
    }
}
