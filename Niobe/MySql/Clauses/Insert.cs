﻿using System.Collections.Generic;

namespace Niobe.MySql.Clauses {
    internal class Insert : AbstractClause {
        public override string component { get; set; } = "insert";

        public List<string> Columns = new List<string>();

        public List<object> Values = new List<object>();
    }
}