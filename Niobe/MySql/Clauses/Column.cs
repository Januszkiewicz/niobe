﻿namespace Niobe.MySql.Clauses {
    internal class Column : AbstractClause {
        public override string component { get; set; } = "from";

        public string name { get; set; }

        public override string ToString() {
            return this.name;
        }
    }
}