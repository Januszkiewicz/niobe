﻿namespace Niobe.MySql.Clauses {
    internal class Limit : AbstractClause {
        public override string component { get; set; } = "limit";

        public int amount { get; set; } = 0;

        public int? offset { get; set; } = 0;

        public override string ToString() {
            return this.amount != 0 ? "LIMIT " + (this.offset != 0 ? this.amount.ToString() + ", " + this.offset.ToString() : this.amount.ToString()) : null;
        }
    }
}