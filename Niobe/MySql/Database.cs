﻿using MySql.Data.MySqlClient;

using System;

using System.Collections.Generic;

using System.Xml;

namespace Niobe.MySql {
    public class Database {
        /* Instance private variables */

        private readonly string ConnectionString = "";

        /* Instance variables */

        public MySqlConnection Connection = null;

        public SshConnection SshConnection = null;

        public QueryCompiler compiler = new QueryCompiler();

        /* Constructor method */

        public Database(XmlNode node) {
            if(node == null) {
                return;
            }

            foreach(XmlAttribute attribute in node.Attributes) {
                this.ConnectionString += attribute.Name + "=" + attribute.Value + ";";
            }

            this.Connection = new MySqlConnection(this.ConnectionString);

            if(node.HasChildNodes) {
                if(node.FirstChild.Name == "ssh_tunnel") {
                    this.SshConnection = new SshConnection(node.FirstChild);
                }
            }
        }

        /* Instance methods */

        public Query Query(string table) {
            return new Query(table);
        }

        public long Insert(Query query) {
            Compiler.Result result = this.compiler.Compile(query);

            if(this.Connection.State != System.Data.ConnectionState.Open) {
                this.Connection.Open();
            }

            MySqlCommand command = this.Connection.CreateCommand();

            command.CommandText = result.Sql;

            if(0 < result.Bindings.Count) {
                this.BindParameters(ref command, result.NamedBindings);
            }

            command.ExecuteNonQuery();

            if(this.Connection.State != System.Data.ConnectionState.Closed) {
                this.Connection.Close();
            }

            return command.LastInsertedId;
        }

        public T Find<T>(Query query) {
            query.Limit(1);

            Compiler.Result result = this.compiler.Compile(query);

            if(this.Connection.State != System.Data.ConnectionState.Open) {
                this.Connection.Open();
            }

            MySqlCommand command = this.Connection.CreateCommand();

            command.CommandText = result.Sql;

            if(0 < result.Bindings.Count) {
                this.BindParameters(ref command, result.NamedBindings);
            }

            T item = default(T);

            using(MySqlDataReader reader = command.ExecuteReader()) {
                while(reader.Read()) {
                    item = this.InstantiateRow<T>(reader);
                }

                reader.Close();
            }

            if(this.Connection.State != System.Data.ConnectionState.Closed) {
                this.Connection.Close();
            }

            return item;
        }

        public List<T> FindAll<T>(Query query, object parameters = null) {
            Compiler.Result result = this.compiler.Compile(query);

            if(this.Connection.State != System.Data.ConnectionState.Open) {
                this.Connection.Open();
            }

            MySqlCommand command = this.Connection.CreateCommand();

            command.CommandText = result.Sql;

            if(0 < result.Bindings.Count) {
                this.BindParameters(ref command, result.NamedBindings);
            }

            List<T> items = new List<T>();

            using(MySqlDataReader reader = command.ExecuteReader()) {
                while(reader.Read()) {
                    items.Add(this.InstantiateRow<T>(reader));
                }

                reader.Close();
            }

            return items;
        }

        private T InstantiateRow<T>(MySqlDataReader reader) {
            T item = (T)Activator.CreateInstance(typeof(T));

            for(int ordinal = 0; ordinal < reader.FieldCount; ++ordinal) {
                string name = reader.GetName(ordinal);

                if(reader.IsDBNull(ordinal)) {
                    item.GetType().GetProperty(name).SetValue(item, null);
                }
                else {
                    item.GetType().GetProperty(name).SetValue(item, reader.GetValue(ordinal));
                }
            }

            return item;
        }

        private void BindParameters(ref MySqlCommand command, Dictionary<string, object> parameters) {
            foreach(KeyValuePair<string, object> parameter in parameters) {
                command.Parameters.AddWithValue(parameter.Key, parameter.Value);
            }
        }
    }
}
