﻿using System.IO;

namespace Niobe {
    public class Image {
        /* Static methods */

        public static bool IsFormat(string source, System.Drawing.Imaging.ImageFormat format) {
            using(FileStream stream = File.Open(source, FileMode.Open)) {
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                if(!image.RawFormat.Equals(format)) {
                    return false;
                }
            }

            return true;
        }

        public static bool Save(string source, string destination, System.Drawing.Imaging.ImageFormat format) {
            System.Drawing.Image image = System.Drawing.Image.FromFile(source);

            image.Save(destination, format);

            if(image != null) {
                image.Dispose();
            }

            return true;
        }
    }
}
